class CanvasStructure {

    private int width;
    private int height;
    private int x1, y1, x2,y2,x,y;
    private char ch;

    public CanvasStructure(int width, int height) {
        this.height = height;
        this.width = width;
    }

    public CanvasStructure draw(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
        return this;
    }

    public CanvasStructure fill(int x, int y, char ch) {
        this.x = x;
        this.y = y;
        this.ch = ch;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getx1(){
        return x1;
    }
    public int getx2(){
        return x2;
    }
    public int gety1(){
        return y1;
    }
    public int gety2(){
        return y2;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getCh() {
        return ch;
    }


}