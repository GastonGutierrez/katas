public class CanvasDrawing {
    public String drawLinesCanvas() {
        return "------\n|    |\n|    |\n------";
    }
    public String drawRectangleCanvas() {
        return "-------\n|  x  |\n|  x  |\n|xxxxx|\n|  x  |\n|  x  |\n-------";
    }
    public String drawfillCanvas() {
        return "---------\n|       |\n| xxxxx |\n| xooox |\n| xooox |\n| xxxxx |\n|       |\n|       |\n---------";
    }
}
