import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SolutionTest {
    CanvasDrawing canvasDrawing = new CanvasDrawing();
    @Test
    public void drawLines() {
        CanvasStructure canvasStructure = new CanvasStructure(2, 2);
        canvasStructure.draw(0, 2, 4, 2);
        assertEquals("------\n|    |\n|    |\n------", canvasDrawing.drawLinesCanvas());
        assertEquals(2,canvasStructure.getHeight());
        assertEquals(2,canvasStructure.getWidth());
        assertEquals(0,canvasStructure.getx1());
        assertEquals(4, canvasStructure.getx2());
        System.out.println(canvasDrawing.drawLinesCanvas());

    }

    @Test
    public void drawRectangle() {
        CanvasStructure canvasStructure = new CanvasStructure(5, 5);
        canvasStructure.draw(0, 2, 4, 2).draw(2, 0, 2, 4);
        assertEquals("-------\n|  x  |\n|  x  |\n|xxxxx|\n|  x  |\n|  x  |\n-------", canvasDrawing.drawRectangleCanvas());
        assertEquals(5,canvasStructure.getHeight());
        assertEquals(5,canvasStructure.getWidth());
        assertEquals(0,canvasStructure.gety1());
        assertEquals(4, canvasStructure.gety2());
        System.out.println(canvasDrawing.drawRectangleCanvas());

    }

    @Test
    public void fill() {
        CanvasStructure canvasStructure = new CanvasStructure(7, 7);
        canvasStructure.draw(1, 1, 5, 4).fill(3, 3, 'o');
        assertEquals("---------\n|       |\n| xxxxx |\n| xooox |\n| xooox |\n| xxxxx |\n|       |\n|       |\n---------", canvasDrawing.drawfillCanvas());
        assertEquals(7,canvasStructure.getHeight());
        assertEquals(7,canvasStructure.getWidth());
        assertEquals('o',canvasStructure.getCh());
        assertEquals(3,canvasStructure.getX());
        assertEquals(3,canvasStructure.getY());
        System.out.println(canvasDrawing.drawfillCanvas());

    }
}