package LombokBulder5;


import java.util.Arrays;

public class Rotator {

    private int[] data = new int[]{};
    private int numberRotate = 0;
    private int numberLength = 0;

    public void rotate(int[] data, int numberRotate) {

        this.data = data;
        this.numberRotate = numberRotate;
        this.numberLength = numberLength;

        int length = data.length;
        numberRotate %= length;
        Object[] result = new Object[length];
        for (int i = 0; i < data.length; i++) {
            numberLength = i + numberRotate;
            if (numberLength > length - 1) {
                numberLength %= length;

            } else if
            (numberLength < 0) numberLength += length;
            result[numberLength] = data[i];
        }
    }


    public String getData() {
        return Arrays.toString(data);
    }

    public int getnumberRotate() {
        return numberRotate;
    }

    public int getnumberLength() {
        return data.length;
    }
}
