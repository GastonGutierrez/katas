public class People{
    private int age;
    private String name;
    private String lastName;

    public People(int age, String name, String lastName ) {
        this.age = age;
        this.name = name;
        this.lastName = lastName;
    }
    Printer printer = new Printer();

    public String greet(){
        return printer.getGREET()+name + lastName;
    }
    public String age(){
        return printer.getAGE()+age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }
}
