public class main {
    public static void main(String[] arg) {
        Printer printer = new Printer();

        People people = new People(12,"juan","Sebastian");
        printer.printerMessage(people.getName());
        printer.printerMessage(people.getLastName());
        printer.printerMessage(String.valueOf(people.getAge()));
        printer.printerMessage(people.greet());
        printer.printerMessage(people.age());

    }
}
