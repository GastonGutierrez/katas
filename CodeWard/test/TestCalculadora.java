 import Stack.Calculadora;
 import Stack.Stack;
 import Stack.Interprete;
 import Stack.Add;
 import Stack.Multiplication;
 import Stack.Factorial;
 import Stack.Potencia;
 import junit.framework.TestCase;



 

 public class TestCalculadora extends TestCase  {
     // son test de las clases de cada operacion
     public void testsuma() {

         Add add = new Add();
         Stack<Character> characterStack = new Interprete().getTokens("2+3+10+9+100");
         Stack<Character> result = add.operationAdd(characterStack);
         String quotes = "";
         while (result.size() > 0) {
             quotes += result.pop().toString();
         }
         int expected = Integer.valueOf(quotes);
         assertEquals(expected, 124);
     }
    public void testsuma2() {

        Add add = new Add();
        Stack<Character> characterStack = new Interprete().getTokens("222+900+100");
        Stack<Character> result = add.operationAdd(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 1222);
    }
    public void testsuma3() {

        Add add = new Add();
        Stack<Character> characterStack = new Interprete().getTokens("221+34+576");
        Stack<Character> result = add.operationAdd(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 831);
    }

    public void testMulti() {

        Multiplication multiplication = new Multiplication();
        Stack<Character> characterStack = new Interprete().getTokens("2*5*10");
        Stack<Character> result = multiplication.operationMultiplicacion(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 100);
    }
    public void testMulti2() {

        Multiplication multiplication = new Multiplication();
        Stack<Character> characterStack = new Interprete().getTokens("20*12*10");
        Stack<Character> result = multiplication.operationMultiplicacion(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 2400);
    }
    public void testMulti3() {

        Multiplication multiplication = new Multiplication();
        Stack<Character> characterStack = new Interprete().getTokens("2*100*2");
        Stack<Character> result = multiplication.operationMultiplicacion(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 400);
    }

    public void testFactor() {

        Factorial factorial = new Factorial();
        Stack<Character> characterStack = new Interprete().getTokens("5!");
        Stack<Character> result = factorial.Facto(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 120);
    }
    public void testFactor2() {

        Factorial factorial = new Factorial();
        Stack<Character> characterStack = new Interprete().getTokens("10!");
        Stack<Character> result = factorial.Facto(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 3628800);
    }
    public void testFactor3() {

        Factorial factorial = new Factorial();
        Stack<Character> characterStack = new Interprete().getTokens("15!");
        Stack<Character> result = factorial.Facto(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 2004310016);
    }


    public void testPoten() {

        Potencia potencia = new Potencia();
        Stack<Character> characterStack = new Interprete().getTokens("5^2^2");
        Stack<Character> result = potencia.power(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 625);
    }
    public void testPoten2() {

        Potencia potencia = new Potencia();
        Stack<Character> characterStack = new Interprete().getTokens("3^2");
        Stack<Character> result = potencia.power(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 9);
    }
    public void testPoten3() {

        Potencia potencia = new Potencia();
        Stack<Character> characterStack = new Interprete().getTokens("3^4^5");
        Stack<Character> result = potencia.power(characterStack);
        String quotes = "";
        while (result.size() > 0) {
            quotes += result.pop().toString();
        }
        int expected = Integer.valueOf(quotes);
        assertEquals(expected, 2147483647);
    }

    // los test son desde mi clase calculadora

    public void testSuma() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("1+2+4+9+9+9");
        int expected = 34;
        assertEquals(expected, actual);
    }

    public void testSuma2() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("9+2+1");
        int expected = 12;
        assertEquals(expected, actual);
    }

    public void testMultiplicacion() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("1*5*2*10");
        int expected = 100;
        assertEquals(expected, actual);
    }

    public void testMultiplicacion2() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("4*2*5");
        int expected = 40;
        assertEquals(expected, actual);
    }

    public void testFactorial() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("2!");
        int expected = 2;
        assertEquals(expected, actual);
    }

    public void testFactorial2() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("5!");
        int expected = 120;
        assertEquals(expected, actual);
    }

    public void testPotencia() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("2^3");
        int expected = 8;
        assertEquals(expected, actual);

    }

    public void testPotencia2() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("5^2");
        int expected = 25;
        assertEquals(expected, actual);

    }

    public void testCombinacion() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("1+2+3*2*2");
        int expected = 15;
        assertEquals(expected, actual);
    }

    public void testCombinacion2() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("2*10+3");
        int expected = 23;
        assertEquals(expected, actual);
    }

    public void testCombinacion3() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("4+2!");
        int expected = 720;
        assertEquals(expected, actual);
    }

    public void testCombinacion4() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("1+4*2!");
        int expected = 5040;
        assertEquals(expected, actual);
    }

    public void testCombinacion5() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("2+2^2");
        int expected = 16;
        assertEquals(expected, actual);
    }

    public void testCombinacion6() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("5+2+2!");
        int expected = 362880;
        assertEquals(expected, actual);
    }

    public void testCombinacion7() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("2!+3*2+2^2");
        int expected = 12;
        assertEquals(expected, actual);
    }
    public void testCombinacion9() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("3+2*2");
        int expected = 7;
        assertEquals(expected, actual);
    }
    // empieso a evaluar los parentecis

    public void testParentecis() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("(2+2)");
        int expected = 4;
        assertEquals(expected, actual);
    }
    public void testParentecis2() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("(2*3)");
        int expected = 6;
        assertEquals(expected, actual);
    }
    public void testParentecis3() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("(5!)");
        int expected = 120;
        assertEquals(expected, actual);
    }
    public void testParentecis4() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("(2^3)");
        int expected = 8;
        assertEquals(expected, actual);
    }
    public void testParentecis5() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("(2+3)*2");
        int expected = 10;
        assertEquals(expected, actual);
    }
    public void testParentecis6() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("2+(2+3)");
        int expected = 7;
        assertEquals(expected, actual);
    }
    public void testParentecis7() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("5*(2*3)");
        int expected = 30;
        assertEquals(expected, actual);
    }
    public void testParentecis8() {
        Calculadora calculadora = new Calculadora();
        int actual = calculadora.Calculadora("(2^3)+2");
        int expected = 10;
        assertEquals(expected, actual);
    }



}
