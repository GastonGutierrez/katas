import JUArrayList.JUArrayList;
import org.junit.*;


public class TestJUArrayList {

    private JUArrayList juArrayList;

    @Before
    public void setUp() {
        this.juArrayList = new JUArrayList();
    }

    @After
    public void tearDown() {
        this.juArrayList = null;
    }

    @org.junit.Test
    public void size() {
        assertArrayEquals( new Object[] {5, 1, 2, 3, 4} );
        juArrayList.arrays(new int[]{5, 1, 2, 3, 4});
        juArrayList.size();
        System.out.println(juArrayList.size());
    }
    @org.junit.Test
    public void isEmpty() {
        assertArrayEquals( new Object[] {} );
        juArrayList.arrays(new int[]{});
        juArrayList.isEmpty();
    }
    @org.junit.Test
    public void iterator() {
        assertArrayEquals( new Object[] {1,2,3} );
        juArrayList.arrays(new int[]{1,2,3});
        juArrayList.iterator();
        System.out.println(juArrayList.iterator());
    }
    @org.junit.Test

    public void add() {
        assertArrayEquals( new Object[] {1,2,3} );
        juArrayList.arrays(new int[]{1,2,3});

        juArrayList.add(1,2);
    }
    @org.junit.Test

    public void objectRemove() {
        assertArrayEquals( new Object[] {1,2,3} );
        juArrayList.arrays(new int[]{1,2,3});

        juArrayList.remove(1);
    }
    @org.junit.Test

    public void intRemove() {
        assertArrayEquals( new Object[] {1,2,3} );
        juArrayList.arrays(new int[]{1,2,3});

        juArrayList.remove(1);
    }
    @org.junit.Test

    public void clear() {
        assertArrayEquals( new Object[] {1,2,3} );
        juArrayList.arrays(new int[]{1,2,3});
        juArrayList.clear();
    }
    @org.junit.Test

    public void get() {
        assertArrayEquals( new Object[] {1,2,3} );
        juArrayList.arrays(new int[]{1,2,3});
        juArrayList.get(1);
        System.out.println(juArrayList.get(1));
    }

    private void assertArrayEquals(Object[] objects) {
    }

}

