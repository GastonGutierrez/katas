import static org.junit.Assert.assertNull;

import linkedList.Append;
import org.junit.Test;

public class testAppend {

    @Test
    public void twoEmpty() throws Exception {
        assertNull( Append.append( null, null ) );
    }

    @Test
    public void oneEmpty() throws Exception {
        testAppend.assertEquals( Append.append( null, new Append( 1 ) ), new Append( 1 ) );
        testAppend.assertEquals( Append.append( new Append( 1 ), null ), new Append( 1 ) );
    }

    private static void assertEquals(Append append, Append append1) {
    }

    @Test
    public void oneOne() throws Exception {
        testAppend.assertEquals( Append.append( new Append( 1 ), new Append( 2 ) ), testAppend.build( new int[] { 1, 2 } ) );
        testAppend.assertEquals( Append.append( new Append( 2 ), new Append( 1 ) ), testAppend.build( new int[] { 2, 1 } ) );
    }

    private static Append build(int[] ints) {
        return null;
    }

    @Test
    public void bigLists() throws Exception {
        testAppend.assertEquals(
                Append.append( testAppend.build( new int[] { 1, 2 } ), testAppend.build( new int[] { 3, 4 } ) ),
                testAppend.build( new int[] { 1, 2, 3, 4 } )
        );
        testAppend.assertEquals(
                Append.append( testAppend.build( new int[] { 1, 2, 3, 4, 5 } ), testAppend.build( new int[] { 6, 7, 8 } ) ),
                testAppend.build( new int[] { 1, 2, 3, 4, 5, 6, 7, 8 } )
        );
    }

}
