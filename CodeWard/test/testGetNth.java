
import linkedList.GetNth;
import static org.junit.Assert.*;
import org.junit.Test;

public class testGetNth {

    @Test
    public void test2() {
        GetNth n = new GetNth();
        n.data = 1337;
        n.next = new GetNth();
        n.next.data = 42;
        n.next.next = new GetNth();
        n.next.next.data = 23;
        try{
            assertEquals(GetNth.getNth(n, 0), 1337);
            assertEquals(GetNth.getNth(n, 1), 42);
            assertEquals(GetNth.getNth(n, 2), 23);
        }catch(Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void testNull() {
        try{
            GetNth.getNth(null, 0);
            assertTrue(false);
        }catch(Exception e){
            assertTrue(true);
        }
    }


    @Test
    public void testWrongIdx() {
        try{
            GetNth.getNth(new GetNth(), 1);
            assertTrue(false);
        }catch(Exception e){
            assertTrue(true);
        }
    }
}

