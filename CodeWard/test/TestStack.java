import Stack.Stack;
import junit.framework.TestCase;


public class TestStack extends TestCase {


    public void testPush(){
        Stack<Integer> stack = new Stack();
        stack.push(1);
        stack.push(2);
        stack.push(1);

        assertEquals(3, stack.size());

    }



    

    public void testPush2(){
        Stack<String> stack = new Stack();
        stack.push("auto");
        stack.push("gato");
        stack.push("casa");
        stack.push("sal");

        assertEquals(4, stack.size());
    }
    public void testPop(){
        Stack<String> stack = new Stack();
        stack.push("auto");
        stack.push("gato");
        stack.push("casa");
        stack.push("sal");
        stack.pop();

        assertEquals(3, stack.size());
    }
    public void testPop2(){
        Stack<Integer> stack = new Stack();
        stack.push(2);
        stack.push(4);
        stack.push(5);
        stack.push(2);
        stack.pop();
        assertEquals(3, stack.size());
    }
    public void testPopAndPush(){
        Stack<Integer> stack = new Stack();
        stack.push(2);
        stack.push(4);
        stack.pop();
        assertEquals(1, stack.size());
        stack.push(5);
        stack.push(2);
        stack.pop();
        assertEquals(2, stack.size());
        stack.push(7);
        assertEquals(3, stack.size());
    }
    public void testPopAndPush2(){
        Stack<String> stack = new Stack();
        stack.push("casa");
        stack.pop();
        assertEquals(0, stack.size());
        stack.push("auto");
        assertEquals(1, stack.size());
        stack.push("mesa");
        stack.push("sal");
        stack.pop();
        assertEquals(2, stack.size());
        stack.push("gato");
        assertEquals(3, stack.size());
        stack.pop();
        assertEquals(2, stack.size());

    }



}
