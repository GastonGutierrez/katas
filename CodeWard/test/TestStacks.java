import junit.framework.TestCase;
import stack.Stack;


public class TestStacks extends TestCase {


    public void testPush(){
        Stack<Integer> stack = new Stack();
        stack.push(1);
        stack.push(2);
        stack.push(1);

        assertEquals(3, stack.size());

    }

    public void testPush2(){
        Stack<String> stack = new Stack();
        stack.push("auto");
        stack.push("gato");
        stack.push("casa");
        stack.push("sal");

        assertEquals(4, stack.size());
    }
    public void testPop(){
        Stack<String> stack = new Stack();
        stack.push("auto");
        stack.push("gato");
        stack.push("casa");
        stack.push("sal");


        assertEquals("sal", stack.pop());
        assertEquals("casa", stack.pop());
    }
    public void testPop2(){
        Stack<Integer> stack = new Stack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        assertEquals(Integer.valueOf(4),stack.pop());
        assertEquals(Integer.valueOf(3),stack.pop());

    }
    public void testPopAndPush(){
        Stack<Integer> stack = new Stack();
        stack.push(2);
        stack.push(4);
        stack.pop();
        assertEquals(1, stack.size());
        stack.push(5);
        stack.push(2);
        stack.pop();
        assertEquals(2, stack.size());
        stack.push(7);
        assertEquals(3, stack.size());
    }
    public void testPopAndPush2(){
        Stack<String> stack = new Stack();
        stack.push("casa");
        stack.pop();
        assertEquals(0, stack.size());
        stack.push("auto");
        assertEquals(1, stack.size());
        stack.push("mesa");
        stack.push("sal");
        stack.pop();
        assertEquals(2, stack.size());
        stack.push("gato");
        assertEquals(3, stack.size());
        stack.pop();
        assertEquals(2, stack.size());

    }
    public void testPopAndPush3(){
        Stack<String> stack = new Stack();
        stack.push("casa");
        assertEquals("casa", stack.pop());
        stack.push("auto");
        assertEquals(1, stack.size());
        stack.push("mesa");
        stack.push("sal");
        assertEquals("sal", stack.pop());
        stack.push("gato");
        assertEquals(3, stack.size());
        assertEquals("gato", stack.pop());

    }

    public void testPopAndPush4(){
        Stack<Integer> stack = new Stack();
        stack.push(2);
        stack.push(4);
        assertEquals(Integer.valueOf(4), stack.pop());
        assertEquals(1, stack.size());
        stack.push(5);
        stack.push(2);
        assertEquals(Integer.valueOf(2), stack.pop());
        assertEquals(2, stack.size());
        stack.push(7);
        assertEquals(3, stack.size());
        assertEquals(Integer.valueOf(7), stack.pop());
    }

}
