



import DoublyCircularLinkedList.DoublyCircularLinkedList;
import DoublyCircularLinkedList.List;
import junit.framework.TestCase;

public class TestDoublyCircularLinkedList extends TestCase{


    public void testGet(){

        List<String> list = new DoublyCircularLinkedList<>();

        list.add("auto");
        list.add("arbol");
        list.add("gato");
        System.out.println(list);

        assertEquals("auto", list.get(1));

    }
    public void testAddAndRemove(){

        List<String> list = new DoublyCircularLinkedList<>();

        list.add("auto");
        list.add("arbol");
        list.add("gato");
        System.out.println(list);
        assertEquals(3, list.size());
        list.add("casa");
        list.add("oso");
        System.out.println(list);
        assertEquals(5, list.size());
        assertEquals(true, list.remove("auto"));
        System.out.println(list);
        assertEquals(4, list.size());
        assertEquals(true, list.remove("oso"));
        System.out.println(list);
        assertEquals(3, list.size());

    }
    public void testAddAndRemove2(){

        List<Integer> list = new DoublyCircularLinkedList<>();

        list.add(2);
        list.add(3);
        list.add(1);
        System.out.println(list);
        assertEquals(true, list.remove(1));

        assertEquals(2, list.size());

        list.add(3);
        System.out.println(list);
        assertEquals(3, list.size());
        assertEquals(true, list.remove(3));
        list.add(5);
        System.out.println(list);
        assertEquals(3, list.size());
        System.out.println(list);
        assertEquals(true, list.remove(2));
        System.out.println(list);
        assertEquals(2, list.size());

    }


    public void testAdd(){

        List<String> list = new DoublyCircularLinkedList<>();

        list.add("auto");
        list.add("arbol");
        list.add("gato");
        System.out.println(list);

        assertEquals(3, list.size());

    }
    public void testRemove(){

        List<String> list = new DoublyCircularLinkedList<>();

        list.add("auto");
        list.add("arbol");
        list.add("gato");
        System.out.println(list);

        assertEquals(true, list.remove("auto"));
        System.out.println(list);


    }






    public void testRemove2(){

        List<Integer> list = new DoublyCircularLinkedList<>();
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println(list);

        assertEquals(false, list.remove(1));


    }
    public void testAdd2(){

        List<Integer> list = new DoublyCircularLinkedList<>();

        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println(list);

        assertEquals(3, list.size());

    }


    public void testXchange(){

        List<Integer> list = new DoublyCircularLinkedList<>();
        list.add(1); //0
        list.add(2); //1
        list.add(3); //2
        list.add(4); //3
        list.add(5); //4
        list.add(6); //5
        System.out.println(list);
        assertEquals("2", list.get(1));

        list.xchange(1,4);
        assertEquals("3", list.get(1));


        System.out.println(list);

    }

    public void testXchange2(){

        List<String> list = new DoublyCircularLinkedList<>();
        list.add("auto"); //0
        list.add("arbol"); //1
        list.add("gato"); //2
        list.add("casa");//3
        list.add("oso"); //4

        System.out.println(list);
        assertEquals("arbol", list.get(1));

        list.xchange(1,4);
        assertEquals("oso", list.get(1));

        System.out.println(list);

    }







    public void testsize(){
        List<Integer> list = new DoublyCircularLinkedList<>();
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println(list);

        assertEquals(3, list.size());

    }
    public void testIsEmpy(){
        List<Integer> list = new DoublyCircularLinkedList<>();
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.println(list);

        assertEquals(true, list.isEmpty());

    }


}
