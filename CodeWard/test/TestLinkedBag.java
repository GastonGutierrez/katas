import LinkedBag.LinkedBag;
import junit.framework.TestCase;



public class TestLinkedBag extends TestCase {


    public void testSelection() {

        System.out.println("selection");

        LinkedBag<String> LinkedBag = new LinkedBag<String>();
        long startTime = System.nanoTime();

        LinkedBag.add("C");
        LinkedBag.add("V");
        LinkedBag.add("B");
        LinkedBag.add("Z");


        System.out.println(LinkedBag);

        LinkedBag.selectionSort();

        System.out.println(LinkedBag);

        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        assertEquals("B", LinkedBag.get(0));


        System.out.println("Elapsed Time: " + timeElapsed);

    }

    public void testSelection2() {

        LinkedBag<Integer> LinkedBag = new LinkedBag<Integer>();

        LinkedBag.add(1);
        LinkedBag.add(4);
        LinkedBag.add(7);
        LinkedBag.add(5);
        assertEquals("1", LinkedBag.get(0));


        LinkedBag.bubbleSort();

    }

    public void testBubbles() {

        System.out.println("bubbles");

        LinkedBag<String> LinkedBag = new LinkedBag<String>();
        long startTime = System.nanoTime();

        LinkedBag.add("C");
        LinkedBag.add("V");
        LinkedBag.add("B");
        LinkedBag.add("Z");

        System.out.println(LinkedBag);

        LinkedBag.bubbleSort();

        System.out.println(LinkedBag);

        assertEquals("B", LinkedBag.get(0));


        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;

        System.out.println("Elapsed Time: " + timeElapsed);

    }

    public void testBubbles2() {

        LinkedBag<Integer> LinkedBag = new LinkedBag<Integer>();

        LinkedBag.add(1);
        LinkedBag.add(4);
        LinkedBag.add(7);
        LinkedBag.add(5);
        assertEquals("1", LinkedBag.get(0));


        LinkedBag.bubbleSort();


    }

}