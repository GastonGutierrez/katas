package Practica;

public class List<T extends Comparable<T>> {
    
    private Node<T>  inicio,fin;
    private int size;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private boolean isEmpy = false;

    public List(){
        inicio = null;
        fin = null;
    }
    public void add(T data){
        Node<T> auxi = new Node<T>(data);

        if(!datosIdenticos(data)){
            if (size == 0){
                fin = auxi;
                inicio = auxi;
                size ++;
            }
            else {
                auxi.setNext(fin);
                fin = auxi;
                size++;
            }
        }

    }



    private boolean datosIdenticos(T data){

        boolean comparar = false;
        Node<T> node = fin;
        for (int i = 0; i < getSize(); i++) {
            if (node.getData().compareTo(data) == 0){
                comparar = true;
            }
            node = node.getNext();
        }
        return  comparar;
    }


    public T deque(){

        T dataRemoved = inicio.getData();
        for (Node<T> listNode = fin; fin.getNext() != null; listNode = listNode.getNext()){
            if (listNode.getNext() == inicio){
                listNode.setNext(null);
                inicio = listNode;
                isEmpy = true;
                break;
            }
        }
        if (fin.getNext() == null && !isEmpy){
            fin = null;
            inicio = null;
        }
        size--;
        return dataRemoved;
    }



    public String toString() { return "("+size+")"+"data=" + fin ; }


}
