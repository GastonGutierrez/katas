package Game;

public class PrinterMessage {

    public void printerMessage(String message){
        System.out.println(message);
    }

    private String greet = "hello nice too meet you :)" +"\n"+ " welcome to super obstacles game "+"\n";
    private String gameInstructions = "This game is very entertaining the " +"\n"+
            "goal is to reach the lower end of the game dodging the various obstacles " +"\n"+
            "that are there to move up is with the key (w) to move down with the key (s), " +"\n"+
            "to move to the right with the key (a), and to the left with the key (d)." +"\n"+
            " Have fun in this fabulous game."+"\n";
    private String farewell = "thank you for playing and we look forward to seeing you soon.";

    public String getGreet() {
        return greet;
    }

    public String getGameInstructions() {
        return gameInstructions;
    }

    public String getFarewell() {
        return farewell;
    }

}
