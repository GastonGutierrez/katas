package Game;

import java.util.Arrays;
import java.util.Random;

public class MapStructure {

    private Random random = new Random();
    private String[][] map = new String[10][10];
    public MapStructure() {

        for (String[] array : map){
            Arrays.fill(array, " ");
        }
        obstaculs();
        playerPosition();
    }
    public void obstaculs(){
        int n = 10;
        while (n>0){
            int randomNumX = random.nextInt(map.length);
            int randomNumY = random.nextInt(map.length);
            map[randomNumY][randomNumX]= "X";
            n--;
        }
    }

    private void playerPosition(){
        map[0][0] = "0";
    }
    public void PrintMap() {

        for (String[] strings : map) {
            System.out.println(Arrays.toString(strings)+"\n");
        }
    }
    public String[][] getMap() {
        return map;
    }
}