package LinkedBag;

public class LinkedBag<T extends Comparable<T> > implements Bag<T> {

    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }

    public LinkedBag() {
        root = null;
    }
    public Node<T> getRoot(int i) {
        return root;
    }

    public int getSize() {
        return size;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }
    private int Comparar(Object obj1, Object obj2) {
        return obj1.toString().compareTo(obj2.toString());
    }

    public void bubbleSort() {

        Node<T> aux1;
        Node<T>  aux2;
        aux2 = root;
        while (aux2 != null) {
            aux1 = aux2.getNext();
            while (aux1 != null) {
                if (Comparar(aux1.getData(), aux2.getData()) < 0) {
                    Object aux = aux2.getData();
                    aux2.setData(aux1.getData());
                    aux1.setData( (T) aux);
                }
                aux1 = aux1.getNext();
            }
            aux2 = aux2.getNext();
        }

    }


    public void selectionSort(){
        Node<T> node = root;
        for (int i = 0; i < size - 1; i++, node = node.getNext()){
            for (int j = i + 1; j < size; j++){
                T a = getRoot(i).getData();
                T b = getRoot(j).getData();
                if(a.compareTo(b)>0){
                    T temp = getRoot(i).getData();
                    Node<T> aux = getRoot(i);
                    aux.setData(b);
                    Node<T> aux2 = getRoot(j);
                    aux2.setData(temp);
                }
            }
        }
    }


    public T get(int index) {
        int contador = 0;
        Node temporal = root;
        while (contador < index){
            temporal = temporal.getNext(); 
            contador++;

        }
        return (T) temporal.getData();
    }



}
