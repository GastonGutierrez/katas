package LinkedBag;

public interface Bag<T extends Comparable<T> > {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();



}