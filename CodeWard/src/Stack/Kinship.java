package Stack;





public class Kinship extends Interprete{
    Factorial factorial = new Factorial();
    Potencia potencia = new Potencia();
    Add add = new Add();
    Multiplication multiplication = new Multiplication();

    public void useParentesis(Character calculatorDigit,Stack<Character> tokens,Stack<Character> auxiliar) {
        if (calculatorDigit == '(') {
            tokens.pop();
            Boolean isCloseKey = true;
            while (auxiliar.size() > 0 && isCloseKey) {
                Character value = auxiliar.pop();

                if (value == '!' && tokens.size() >= auxiliar.size()) {
                    tokens = factorial.Facto(tokens);
                }
                if (value == '^' && tokens.size() >= auxiliar.size()) {
                    tokens = potencia.power(tokens);
                }

                if (value == '+' && tokens.size() >= auxiliar.size()) {
                    tokens = add.operationAdd(tokens);
                }

                if (value == '*' && tokens.size() >= auxiliar.size()) {
                    tokens = multiplication.operationMultiplicacion(tokens);

                }

                if (value == ')') {
                    isCloseKey = false;
                }
            }
        }

    }
}







