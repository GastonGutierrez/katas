package Stack;

public class Calculadora extends Interprete {
    Factorial factorial = new Factorial();
    Potencia potencia = new Potencia();
    Add add = new Add();
    Multiplication multiplication = new Multiplication();

    Kinship kinship = new Kinship();

    public int Calculadora(String str) {

        Stack<Character> tokens = new Interprete().getTokens(str);
        Stack<Character> auxiliar = new Interprete().getTokens(str);

        while (auxiliar.size() > 0) {

            Character calculatorDigit = auxiliar.pop();
            kinship.useParentesis(calculatorDigit,tokens,auxiliar);

            if (calculatorDigit == '!' && tokens.size() >= auxiliar.size()) {
                tokens = factorial.Facto(tokens);
            }
            if (calculatorDigit == '^' && tokens.size() >= auxiliar.size()) {
                tokens = potencia.power(tokens);
            }

            if (calculatorDigit == '+' && tokens.size() >= auxiliar.size()) {
                tokens = add.operationAdd(tokens);
            }

            if (calculatorDigit == '*' && tokens.size() >= auxiliar.size()) {
                tokens = multiplication.operationMultiplicacion(tokens);

            }

        }
        String quotes = "";
        while (tokens.size() > 0) {
            quotes += tokens.pop().toString();

        }

        return Integer.valueOf(quotes);
    }



}
