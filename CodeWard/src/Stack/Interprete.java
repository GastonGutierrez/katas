package Stack;

public class Interprete {
    private Stack<Character> tokens = new Stack<Character>();

    public Interprete() {
    }
    public Stack<Character> getTokens(String str) {
        return getTokens(str, true);
    }

    public Stack<Character> getTokens(String str, boolean isReverse) {
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }

        if (!isReverse){
            return tokens;
        }
        return reverse(tokens);
    }

    private Stack<Character> reverse(Stack<Character> characterStack) {
        Stack<Character> reverse = new Stack<Character>();
        while (characterStack.size() > 0) {
            reverse.push(characterStack.pop());
        }
        return reverse;
    }

    public static String converseNumbersBaseTen(  Stack<Character> tokens, Character value, Boolean isContinueNumber, String tempNumber){
        if (!Character.isDigit(value)){
            value = tokens.pop();
        }

        if(Character.isDigit(value)) {
            while (isContinueNumber){
                tempNumber += value.toString();
                value = tokens.pop();
                if (value == null || !Character.isDigit(value)){
                    isContinueNumber = false;
                }
            }
        }

        if (tempNumber.isEmpty()){
            tempNumber = "0";
        }

        if (value != null && !Character.isDigit(value) && tokens.size() > 0){
            tokens.push(value);
        }

        return tempNumber;
    }

}
