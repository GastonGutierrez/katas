package Stack;

public class Factorial extends Interprete { // factorial ejm: 5! = 5 x 4 x 3 x 2 x 1 = 120
    private Character lastCharacter = null;
    private String tempNumber = "";


    public Stack<Character> Facto(Stack<Character> tokens) {

        int factorial = 1;
        Boolean isSimbolo = true; // Bandera
        Stack<Character> temp = new Stack<Character>();

        while (tokens.size() > 0 && isSimbolo) {
            Character digitfactorial = tokens.pop();
            if (!Character.isDigit(digitfactorial) && !digitfactorial.equals("!".charAt(0))) {
                isSimbolo = false;
                lastCharacter = digitfactorial;
            }
            tempNumber = converseNumbersBaseTen(tokens, digitfactorial, true, tempNumber);

            for (int i = 1; i <= Integer.parseInt(tempNumber); i++) {
                factorial *= i;
            }
        }
        useFactorial(temp, tokens, factorial);
        return tokens;
    }

    private void useFactorial(Stack<Character> temp, Stack<Character> tokens, int factorial) {
        while (tokens.size() > 0) {
            temp.push(tokens.pop());
        }
        Interprete interprete = new Interprete();
        Stack<Character> result = interprete.getTokens(String.valueOf(factorial), false);

        while (result.size() > 0) {
            tokens.push(result.pop());
        }
        if (lastCharacter != null) {
            tokens.push(lastCharacter);
        }
        while (temp.size() > 0) {
            tokens.push(temp.pop());
        }

    }

}

