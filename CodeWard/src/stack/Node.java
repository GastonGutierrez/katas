package stack;

class Node<T> {
    private T data;
    public Node<T> next;

    public Node(T data) {
        this.data = data;
    }
    public T getData() {
        return data;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }
    public Node<T> getNext() {
        return next;
    }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}
