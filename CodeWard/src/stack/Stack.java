package stack;

public class Stack<T extends Comparable<T> > {
    private int size;
    private Node<T> node;

    public int size() {
        return size;
    }

    public void push(T valor){

        Node<T> nuevo = new Node<T>(valor);

        if (node == null){
            node = nuevo;
            node.setNext(null);
        }else {
            nuevo.setNext(node);
            node = nuevo;
        }
        size ++;
    }
    public T pop(){
        if (node != null){
            T data = node.getData();
            node = node.getNext();
            size--;
            return data;
        }
        return null;
    }
    public String toString() {
        return "(" + size + ")" + "root=" + node;
    }



}

