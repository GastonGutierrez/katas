package linkedList;

public class GetNth {
    public int data;
    public GetNth next = null;

    public static int getNth(GetNth n, int index) {
        if (index < 0) throw new IllegalArgumentException();

        GetNth number = n;
        for (int i = 0; i < index; i++) {
            number = number.next;
        }
        return number.data;
    }

}
