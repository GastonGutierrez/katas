package linkedList;

public class Append {

    private int data;
    private Append next = null;

    public Append(int data) {
        this.data = data;
    }

    public static Append append(Append listA, Append listB) {
        if ( listA == null ) {
            return listB;
        }
        Append number = listA;
        while ( number.next != null ) {
            number = number.next;
        }
        number.next = listB;
        return listA;
    }
}