package JULinkedlist;

public class Nodo<T> {


    int dato;
    Nodo<T> sig;

    public Nodo<T> getSig() {
        return sig;
    }

    public <T> Nodo(T dato) {
    }

    public String toString() {
        return " nodo : dato = " + dato + " : siguiente = " + sig;
    }


    public Object obtenerValor() {

        return dato;
    }

    public void enlazarSiguiente(Nodo n) { // una manera de apuntar a otro nodo

        sig = n;
    }

    public Nodo obtenerSiguiente() { // reglesar el enlace acia el siguiente nodo

        return sig;
    }


}
