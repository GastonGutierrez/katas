package Gamer;

import Game.ControlerGamer;
import Game.Player;
import Game.PrinterMessage;

public class Main {
    public static void main(String[] args) {

        PrinterMessage printerMessage = new PrinterMessage();

        Player player = new Player("Gaston",12,"master");
        printerMessage.printerMessage(printerMessage.getGreet());
        printerMessage.printerMessage(printerMessage.getGameInstructions());
        printerMessage.printerMessage("name is: "+ player.getName()+"\n"+ " my nickname is : "+ player.getNickname()+"\n"+"my age is: "
                + player.getAge());

        Game.ControlerGamer controlerGamer = new ControlerGamer();
        controlerGamer.execute();
        printerMessage.printerMessage(printerMessage.getFarewell());

    }
}