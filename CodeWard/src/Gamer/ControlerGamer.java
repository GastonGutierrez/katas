package Gamer;


import Game.MapStructure;

import java.util.Scanner;

public class ControlerGamer extends MapStructure {

    private void moveToup() {
        int playerCoordinateX = playerCoordinates()[0];
        int playerCoordinateY = playerCoordinates()[1];
        if (playerCoordinateY != 0 && !getMap()[playerCoordinateY - 1][playerCoordinateX].equals("x")) {
            getMap()[playerCoordinateY - 1][playerCoordinateX] = "0";
            getMap()[playerCoordinateY][playerCoordinateX] = " ";
        }
    }
    private void moveToDown() {
        int playerCoordinateX = playerCoordinates()[0];
        int playerCoordinateY = playerCoordinates()[1];
        if (playerCoordinateY!= getMap().length - 1 && !getMap()[playerCoordinateY + 1][playerCoordinateX].equals("x")) {
            getMap()[playerCoordinateY + 1][playerCoordinateX] = "0";
            getMap()[playerCoordinateY][playerCoordinateX] = " ";
        }
    }
    private void moveToLeft() {
        int playerCoordinateX = playerCoordinates()[0];
        int playerCoordinateY = playerCoordinates()[1];
        if (playerCoordinateX != 0 && !getMap()[playerCoordinateY][playerCoordinateX - 1].equals("x")) {
            getMap()[playerCoordinateY][playerCoordinateX - 1] = "0";
            getMap()[playerCoordinateY][playerCoordinateX] = " ";
        }
    }
    private void moveToRight() {
        int playerCoordinateX = playerCoordinates()[0];
        int playerCoordinateY = playerCoordinates()[1];
        if (playerCoordinateX != getMap()[0].length - 1 && !getMap()[playerCoordinateY][playerCoordinateX + 1].equals("x")) {
            getMap()[playerCoordinateY][playerCoordinateX + 1] = "0";
            getMap()[playerCoordinateY][playerCoordinateX] = " ";
        }
    }
    private int[] playerCoordinates() {
        int[] playerCoordinates = new int[2];
        for (int y = 0; y < getMap().length; y++) {
            for (int x = 0; x < getMap()[0].length; x++) {
                if (getMap()[y][x] == "0") {
                    playerCoordinates[0] = x;
                    playerCoordinates[1] = y;
                }
            }
        }
        return playerCoordinates;
    }

    public void execute() {
        PrintMap();
        while (!getMap()[getMap().length - 1][getMap()[0].length - 1].equals("0")) {
            controlerComand();
        }
        System.out.println("\n");
    }

    private void controlerComand() {
        Scanner sn = new Scanner(System.in);
        String movementIndex = sn.next();
        sn.nextLine();
        switch (movementIndex) {
            case "w" -> {
                moveToup();
                PrintMap();
                System.out.println("»".repeat(20));
            }
            case "s" -> {
                moveToDown();
                PrintMap();
                System.out.println("»".repeat(20));
            }
            case "d" -> {
                moveToRight();
                PrintMap();
                System.out.println("»".repeat(20));
            }
            case "a" -> {
                moveToLeft();
                PrintMap();
                System.out.println("»".repeat(20));
            }
            default -> System.out.println("no");
        }
    }
}
