package Gamer;

public class Player {

    private String name;

    private String nickname;
    private int age;

    public Player(String name, int age, String nickname) {
        this.name = name;
        this.nickname = nickname;

        age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getNickname() {
        return nickname;
    }
}