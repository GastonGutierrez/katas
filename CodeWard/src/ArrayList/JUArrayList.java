package ArrayList;


import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public  class JUArrayList implements java.util.List{

    private int[] datos = new int[]{};

    static int count =0 ;

    public void arrays(int[] datos) {
        this.datos = datos;

    }

    @Override
    public int size() {
        return datos.length;
    }

    @Override
    public boolean isEmpty() {
        if(datos.length == 0) {
            System.out.println("The array is Empty");
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        for (int i = 0; i < datos.length; i++) {
            System.out.println(i);

        }
        return null;
    }

    @Override
    public boolean add(Object dato) {
        if (count == datos.length) {
            int[] datos2 = new int[datos.length + 10];
            for (int i = 0; i < datos.length; i++) {
                datos2[i] = datos[i];
            }
            datos = datos2;
        }
        datos[count] = (int) dato;
        count++;

        return false;
    }
    @Override
    public boolean remove(Object indice) {

        for (int i = (int) indice; i < count; i++) {
            datos[i] = datos[i + 1];
        }
        count--;
        return false;
    }
    @Override
    public Object remove(int indice) {
        for (int i = indice; i < count; i++) {
            datos[i] = datos[i + 1];
        }
        count--;
        return null;
    }
    @Override
    public Object get(int index) {
        return datos[index];
    }
    @Override
    public void clear() {
        for (int i = 0; i <= datos.length -1; i++) {
            datos[i] = 0;

            System.out.println(datos);
        }

    }








    @Override
    public boolean addAll(Collection collection) {
        return false;
    }

    @Override
    public boolean removeIf(Predicate filter) {
        return List.super.removeIf(filter);
    }

    @Override
    public boolean addAll(int i, Collection collection) {
        return false;
    }

    @Override
    public void replaceAll(UnaryOperator operator) {
        List.super.replaceAll(operator);
    }

    @Override
    public void sort(Comparator c) {
        List.super.sort(c);
    }

    @Override
    public boolean retainAll(Collection collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection collection) {
        return false;
    }

    @Override
    public boolean containsAll(Collection collection) {
        return false;
    }





    @Override
    public Object set(int i, Object o) {
        return null;
    }

    @Override
    public void add(int i, Object o) {

    }
    @Override
    public void forEach(Consumer action) {
        List.super.forEach(action);
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(IntFunction generator) {
        return List.super.toArray(generator);
    }

    @Override
    public Object[] toArray(Object[] objects) {
        return new Object[0];
    }


    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int i) {
        return null;
    }

    @Override
    public List subList(int i, int i1) {
        return null;
    }

    @Override
    public Spliterator spliterator() {
        return List.super.spliterator();
    }

    @Override
    public Stream stream() {
        return List.super.stream();
    }

    @Override
    public Stream parallelStream() {
        return List.super.parallelStream();
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }


}

