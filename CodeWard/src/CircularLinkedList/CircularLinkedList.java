package CircularLinkedList;

class Node<T> {
    private T data;
    public Node<T> next;
    public Node(T data) { this.data = data;}
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() { return "data=" + data; }

    public void setData(T data) {
        this.data = data;
    }



}
public class CircularLinkedList<T> implements List<T> {
    private Node<T> head;
    private Node<T> ultimo;

    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }


    @Override
    public boolean add(T data) {
        Node<T> nuevo = new Node<>(data);
        // Agrega al valor al nodo.
        nuevo.setData(data);
        // Consulta si la lista esta vacia.
        if (isEmpty()) {
            // Inicializa la lista agregando como inicio al nuevo nodo.
            head = nuevo;
            // De igual forma el ultimo nodo sera el nuevo.
            ultimo = nuevo;
            // Y el puntero del ultimo debe apuntar al primero.
            ultimo.setNext(head);
            // Caso contrario el nodo se agrega al final de la lista.
        } else{
            // Apuntamos con el ultimo nodo de la lista al nuevo.
            ultimo.setNext(nuevo);
            // Apuntamos con el nuevo nodo al inicio de la lista.
            nuevo.setNext(head);
            // Como ahora como el nuevo nodo es el ultimo se actualiza
            // la variable ultimo.
            ultimo = nuevo;
        }
        // Incrementa el contador de tamaño de la lista
        size++;

        return true;
    }

    @Override
    public boolean remove(T data) {

        // Consulta si el valor de referencia existe en la lista.
        if (buscar(data)) {
            // Consulta si el nodo a eliminar es el pirmero
            if (head.getData() == data) {
                // El primer nodo apunta al siguiente.
                head = head.getNext();
                // Apuntamos con el ultimo nodo de la lista al inicio.
                ultimo.setNext(head);
            } else{
                // Crea ua copia de la lista.
                Node<T> aux = head;
                // Recorre la lista hasta llegar al nodo anterior
                // al de referencia.
                while(aux.getNext().getData() != data){
                    aux = aux.getNext();
                }
                if (aux.getNext() == ultimo) {
                    aux.setNext(head);
                    ultimo = aux;
                } else {
                    // Guarda el nodo siguiente del nodo a eliminar.
                    Node<T> siguiente = aux.getNext();
                    // Enlaza el nodo anterior al de eliminar con el
                    // sguiente despues de el.
                    aux.setNext(siguiente.getNext());
                    // Actualizamos el puntero del ultimo nodo
                }
            }
            // Disminuye el contador de tamaño de la lista.
            size--;
        }

        return false;
    }
    public boolean buscar(T referencia){
        // Crea una copia de la lista.
        Node<T> aux = head;


        // Bandera para indicar si el valor existe.
        boolean encontrado = false;
        // Recorre la lista hasta encontrar el elemento o hasta
        // llegar al primer nodo nuevamente.
        do{
            // Consulta si el valor del nodo es igual al de referencia.
            if (referencia == aux.getData()){
                // Canbia el valor de la bandera.
                encontrado = true;
            }
            else{
                // Avansa al siguiente. nodo.
                aux = aux.getNext();
            }
        }while(aux != head && encontrado != true);
        // Retorna el resultado de la bandera.
        return encontrado;
    }





    @Override
    public T get(int index) {
        if (isEmpty() || index < 0)
            return null;
        else {
            Node<T> node = head;
            for (int i = 0; i < index ; i++, node = node.getNext());

            return node.getData();
        }
    }

    public static List<Integer> dummyList(int s) {
        CircularLinkedList<Integer> l = new CircularLinkedList<Integer>();
        l.size = s;
        l.head = new Node<>(l.size);
        dummyNode(l.head, --s, null);
        return l;
    }
    private static Node<Integer> dummyNode(Node<Integer> n, int d, Node<Integer> z) {
        z = z != null ? z : n;
        if (d <= 0) { n.setNext(z); return n;}
        else n.setNext(dummyNode(new Node<>(d), d -1, z));
        return  n;
    }
}

