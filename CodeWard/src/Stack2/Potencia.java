package Stack2;

public class Potencia extends Interpreter {
    private Character lastCharacter = null;
    private String baseNumber = "";
    private String expoNumber = "";

    public Stack<Character> power(Stack<Character> tokens) {
        int result = 1;
        Boolean isSimbolo = true;
        Stack<Character> temp = new Stack<Character>();
        while (tokens.size() > 0 && isSimbolo) {
            Character baseDigit = null;
            if (result == 1) {
                baseDigit = tokens.pop();
                baseNumber = converseNumbersBaseTen(tokens, baseDigit, true, baseNumber);
            } else {
                baseNumber = result + "";
            }
            Character expoDigit = tokens.pop();
            expoNumber = converseNumbersBaseTen(tokens, expoDigit, true, expoNumber);

            result = (int) Math.pow(Integer.parseInt(baseNumber), Integer.parseInt(expoNumber));

            baseAndExpoDigit(baseDigit,isSimbolo,expoDigit);

            baseNumber = "";
            expoNumber = "";
        }
        usePower(temp,tokens,result);

        return tokens;

    }

    private void baseAndExpoDigit(Character baseDigit,Boolean isSimbolo, Character expoDigit ){

        if (baseDigit == null || (!Character.isDigit(baseDigit) && !baseDigit.equals("^".charAt(0)))) {
            isSimbolo = false;
            lastCharacter = baseDigit;
        }

        if (expoDigit==null || (!Character.isDigit(expoDigit) && !expoDigit.equals("^".charAt(0)))) {
            isSimbolo = false;
            lastCharacter = expoDigit;
        }

    }


    private void usePower(Stack<Character> temp, Stack<Character> tokens, int potencia){
        while ( tokens.size() > 0 ) {
            temp.push(tokens.pop());
        }
        Interpreter interpreter = new Interpreter();
        Stack<Character> result = interpreter.getTokens(String.valueOf(potencia), false);

        while (result.size() > 0) {
            tokens.push( result.pop());
        }
        if (lastCharacter != null){
            tokens.push(lastCharacter);
        }
        while ( temp.size() > 0 ) {
            tokens.push(temp.pop());
        }

    }
}
