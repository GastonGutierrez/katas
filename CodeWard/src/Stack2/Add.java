package Stack2;

public class Add extends Interpreter {
    private Character lastCharacter = null;

    public Stack<Character> operationAdd(Stack<Character> tokens) {

        int add = 0;
        Boolean isSimbolo = true; // Bandera
        Stack<Character> temp =  new Stack<Character>();
        String tempNumber = "";
        while (tokens.size() > 0 && isSimbolo){
            Character digitAdd = tokens.pop();

            if(!Character.isDigit(digitAdd) && !digitAdd.equals("+".charAt(0))){
                isSimbolo = false;
                lastCharacter = digitAdd;
            }

            tempNumber = converseNumbersBaseTen( tokens, digitAdd, true, tempNumber);

            add += Integer.valueOf(tempNumber);
            tempNumber = "";
        }

        useAdd(temp,tokens,add);
        return tokens;
    }

    private void useAdd(Stack<Character> temp, Stack<Character> tokens, int add){
        while ( tokens.size() > 0 ) {
            temp.push(tokens.pop());
        }
    Interpreter interpreter = new Interpreter();
    Stack<Character> result = interpreter.getTokens(String.valueOf(add), false);

        while (result.size() > 0) {
        tokens.push( result.pop());
    }
        if (lastCharacter != null){
        tokens.push(lastCharacter);
    }
        while ( temp.size() > 0 ) {
        tokens.push(temp.pop());
    }

    }
}
