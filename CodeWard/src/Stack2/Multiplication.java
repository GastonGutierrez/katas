package Stack2;

public class Multiplication extends Interpreter {
    private Character lastCharacter = null;
    public Stack<Character> operationMultiplicacion(Stack<Character> tokens) {

        int multi = 1;
        Boolean isSimbolo = true; // Bandera
        Stack<Character> temp =  new Stack<Character>();
        String tempNumber = "";
        while (tokens.size() > 0 && isSimbolo){
            Character digitMultiplication= tokens.pop();
            if(!Character.isDigit(digitMultiplication) && !digitMultiplication.equals("*".charAt(0))){
                isSimbolo = false;
                lastCharacter = digitMultiplication;
            }
            tempNumber = converseNumbersBaseTen( tokens, digitMultiplication, true, tempNumber);
            multi *= Integer.valueOf(tempNumber);
            tempNumber = "";
        }

        useMultiplication(temp,tokens,multi);

        return tokens;
    }

    private void useMultiplication(Stack<Character> temp, Stack<Character> tokens, int multi){
        while ( tokens.size() > 0 ) {
            temp.push(tokens.pop());
        }
        Interpreter interpreter = new Interpreter();
        Stack<Character> result = interpreter.getTokens(String.valueOf(multi), false);

        while (result.size() > 0) {
            tokens.push( result.pop());
        }
        if (lastCharacter != null){
            tokens.push(lastCharacter);
        }
        while ( temp.size() > 0 ) {
            tokens.push(temp.pop());
        }

    }
}
