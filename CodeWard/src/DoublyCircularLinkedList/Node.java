package DoublyCircularLinkedList;

public class Node<T> {
    private T data;
    private Node<T> next;

    public Node(Comparable data) {
        this.data = (T) data;
        next = this;
    }

    public T getData() {
        return data;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> node) {
        next = node;
    }


}