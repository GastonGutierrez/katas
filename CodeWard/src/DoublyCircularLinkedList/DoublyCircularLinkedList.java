package DoublyCircularLinkedList;

public class DoublyCircularLinkedList<T> implements List{
    private int size;
    private Node<T> first;

    public String toString() {
        Node<T> aux = first;
        String message = "Lista: \n ";
        if (aux != null) {
            message += aux.getData() + ", \n ";
            aux = aux.getNext();
            while (aux != first) {
                message += aux.getData() + ", \n ";
                aux = aux.getNext();
            }
        } else {
            message+= "Vacia";
        }
        return message;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (first != null)? true:false;
    }

    @Override
    public boolean add(Comparable data) {

        Node<T> last = new Node<T>(data);
        if(isEmpty()){
            last.setNext(first.getNext());
            first.setNext(last);

        }
        first = last;
        size ++;

        return false;
    }


    @Override
    public boolean remove(Comparable data) {
        Node<T> last = first;
        Node<T> aux;
        boolean encontrado = false;
        if(isEmpty()){
            while (last.getNext() != first && !encontrado){
                aux = last.getNext();
                encontrado = (aux.getData() == data);
                if(!encontrado){
                    last = last.getNext();
                }
            }
            aux = last.getNext();
            encontrado = (aux.getData() == data);
            if(encontrado){
                if(first == first.getNext()){
                    first = null;
                }else{
                    if(aux == first){

                        first = last;

                    }
                    last.setNext(aux.getNext());
                    aux= null;
                }
            }
        }
        size --;
        return encontrado;
    }

    @Override
    public Comparable get(int index) {
        if(index>=0 && index<size){
            if (index == 0) {
                return (Comparable) first.getData();
            }else{
                Node<T> aux = first;
                for (int i = 0; i < index; i++) {
                    aux = aux.getNext();
                }
                return (Comparable) aux.getData();
            }
        } else {
            System.out.println("Posicion inexistente en la lista.");
        }
        return null;
    }

    @Override
    public void xchange(int x, int y) {

        setNode(x, (T) get(y));
        setNode(y, (T) get(x));
    }


    private Node<T> getPreviousNode(int index , Node<T> node){
        while (index > 0){
            node = node.getNext();
            index --;
        }
        return node;
    }

    private void  setNode(int index, T element){
        Node<T> nodeAux = first;
        Node<T> previus = getPreviousNode(index, nodeAux);
        Node<T> current = new Node<T>((Comparable) element);
        if (index == 0){
            first = previus;
            return;
        }
        else {
            current.setNext(previus.getNext());
            previus.setNext(current);
        }

        first = previus;
    }

}