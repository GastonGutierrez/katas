package recursividad;

public class Main {
    public static void main(String[] args) {
        LlamadasEncadenadas.doOne();

        //Infinito.whileTrue();
        // 2.- a continuacion describir el problema:
        // Descripcion: pues el problema seria que en el metodo de whileTrue existe un while sin condicion ni funcionamiento
        // y de esta manera no sabe cuando detenerse el while


        //Contador.contarHasta(10);
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion: al parecer  el metodo que se encuentra pide un int y tiene in if que verifica si el
        // numero ingresado es mayor a 0 y luego procede a que el metodo valla disminuyendo el contador es
        //decir el numero que hallamos introducido

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));





    }
}