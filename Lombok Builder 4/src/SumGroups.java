import java.util.ArrayDeque;
import java.util.Arrays;

public class SumGroups {

    private int[] arreglo = new int[]{2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9};
    private int lengthNumber = -1;
    private int number = -1;
    Printer printer = new Printer();

    public  void sumGroups() {

        while (lengthNumber != arreglo.length) {

            lengthNumber = arreglo.length;
            ArrayDeque<Integer> next = new ArrayDeque<Integer>();

            printer.printerMessage(printer.getArrangement()+Arrays.toString(arreglo));
            printer.printerMessage(printer.getLength()+lengthNumber);
            printer.printerMessage(printer.getSeparate());

            for (int i = 0 ; i<lengthNumber ; i++) {
                if (number != arreglo[i]%2) {
                    next.add(arreglo[i]);
                    number = arreglo[i]%2;

                } else {
                    next.add(arreglo[i] + next.pollLast());
                }
            }
            arreglo = next.stream().mapToInt( i -> Integer.valueOf(i) ).toArray();
        }
    }
    }